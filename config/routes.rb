Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/site/admin', as: 'rails_admin'
  	root to: "inicio#index"
  	get 'whatsapp/index'
  	get 'inicio/index'
    get 'preco/index'
  	get 'produto/index'
  	get 'produto/index/:id', to: "produto#index"
  	post 'whatsapp/index'
  	resources :whatsapp_infos
  	devise_for :users
end
