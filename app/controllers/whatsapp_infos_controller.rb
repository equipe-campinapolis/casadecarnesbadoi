class WhatsappInfosController < ApplicationController
  before_action :set_whatsapp_info, only: [:show, :edit, :update, :destroy]

  def index
    @whatsapp_infos = WhatsappInfo.all
  end

  def show
  end

  def new
    @whatsapp_info = WhatsappInfo.new
  end

  def edit
  end

  def create
    @whatsapp_info = WhatsappInfo.new(whatsapp_info_params)

    respond_to do |format|
      if @whatsapp_info.save
        format.html { redirect_to @whatsapp_info, notice: 'Whatsapp info was successfully created.' }
        format.json { render :show, status: :created, location: @whatsapp_info }
      else
        format.html { render :new }
        format.json { render json: @whatsapp_info.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @whatsapp_info.update(whatsapp_info_params)
        format.html { redirect_to @whatsapp_info, notice: 'Whatsapp info was successfully updated.' }
        format.json { render :show, status: :ok, location: @whatsapp_info }
      else
        format.html { render :edit }
        format.json { render json: @whatsapp_info.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @whatsapp_info.destroy
    respond_to do |format|
      format.html { redirect_to whatsapp_infos_url, notice: 'Whatsapp info was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_whatsapp_info
      @whatsapp_info = WhatsappInfo.find(params[:id])
    end

    def whatsapp_info_params
      params.require(:whatsapp_info).permit(:to)
    end
end
