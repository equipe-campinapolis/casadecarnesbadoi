class InicioController < ApplicationController

  def index
  	@produtos = Produto.order('random()').all
  	@informacoes = Information.all
    @sobre = About.last
  end

end
