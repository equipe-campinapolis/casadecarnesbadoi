class Produto < ApplicationRecord
enum status: [:ativo, :inativo]
enum category: { "Carne Bovina": 0, "Carne Suína": 1, "Peixes": 2, "Aves": 3, "Carne Assada": 4 , "Outros": 5 }
has_one_attached :image #virtual não precisa estar no banco

# validates :nome, presence: true,:allow_nil => false
# validates :valor, presence: true,:allow_nil => false 
# validates :local, presence: true,:allow_nil => false 
# validates :descricao, presence: true,:allow_nil => false 

#default_scope {where(status: :ativo)}
# scope :em_produtos, -> { where(status: :ativo, local: :produtos) }
# scope :em_promocao,-> { where(sttus: :ativo, local: :promocoes) }

 rails_admin do
     	label " Produtos"
     	label_plural " Produtos"
     	weight 2
     	navigation_icon 'icon-list-alt'
#       edit do
#           configure :image do
#             label "Imagem"
#           end
#           configure :title do
#             label 'Titulo'
#           end
#           configure :description do
#             label 'Descrição'
#           end
#           configure :disable do
#           label "Desabilitar"
#         end
#         configure :imagem do
#           label "Imagem do Produto"
#       	end
#       	configure :nome do
#           label 'Nome'
#       	end
#       	configure :descricao do
#           label 'Descrição'
#       	end
#       	configure :valor do
#           label 'Preço'
#         	#partial "price_partial", valor:10
#         end	
#     end
    end  	
end
