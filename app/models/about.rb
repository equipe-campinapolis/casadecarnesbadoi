class About < ApplicationRecord
  # validates :description, presence: true,:allow_nil => false
	
  before_create :deleta_registro
	def deleta_registro
		About.first.destroy if About.count > 0
	end

	
rails_admin do
      label " Sobre Nós"
      label_plural " Sobre Nós"
      weight 4
      navigation_icon 'icon-pencil'
      edit do
        configure :description do
        label 'Descrição'
        end
      end
      list do
        configure :description do
        label 'Descrição'
        end
        field :description
      end
  end
end