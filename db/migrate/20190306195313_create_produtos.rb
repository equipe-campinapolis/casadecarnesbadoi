class CreateProdutos < ActiveRecord::Migration[5.2]
  def change
    create_table :produtos do |t|
      t.integer :status
      t.string :nome
      t.text :descricao
      t.string :quantidade
      t.decimal :valor
      t.timestamps
    end
  end
end
