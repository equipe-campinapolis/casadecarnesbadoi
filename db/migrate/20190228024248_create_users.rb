class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :nome
      t.string :telefone
      t.string :cpf
      t.integer :tipo
      t.integer :status
      t.text :descricao

      t.timestamps
    end
  end
end
